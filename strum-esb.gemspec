# frozen_string_literal: true

require_relative "lib/strum/esb/version"

Gem::Specification.new do |spec|
  spec.name          = "strum-esb"
  spec.version       = Strum::Esb::VERSION
  spec.authors       = ["Serhiy Nazarov"]
  spec.email         = ["sn@nazarov.com.ua"]

  spec.summary       = "Publish and subscribe rabbitMQ messages"
  spec.description   = "Publish and subscribe rabbitMQ messages"
  spec.homepage      = "https://gitlab.com/strum-rb/strum-esb"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.6.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/strum-rb/strum-esb"
  spec.metadata["changelog_uri"] = "https://gitlab.com/strum-rb/strum-esb/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "bunny", "~> 2.15"
  spec.add_dependency "connection_pool", "~> 2.2.2"
  spec.add_dependency "dry-configurable", "~> 0.12.1"
  spec.add_dependency "dry-inflector", "~> 0.2.1"
  spec.add_dependency "json", "~> 2.3"
  spec.add_dependency "sneakers", "~> 2.12"
end
