# frozen_string_literal: true

module Strum
  module Esb
    VERSION = "0.4.0"
  end
end
