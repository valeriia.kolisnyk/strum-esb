# frozen_string_literal: true

require_relative "message"

module Strum
  module Esb
    # Notice message
    class Notice < Message
      class << self
        def call(payload, notice, resource = nil, exchange: Strum::Esb.config.notice_exchange, **opts)
          headers = { notice: notice }
          headers[:resource] = resource if resource
          publish(headers: headers, payload: payload, exchange: exchange, **opts)
        end
      end
    end
  end
end
