# frozen_string_literal: true

class ProtobufMessage
  def self.decode(_string)
    new("decoded payload")
  end

  def initialize(_payload); end

  def to_proto
    "\b\x01\x12\x04name"
  end

  def to_h
    { id: 1, message: "message" }
  end
end
